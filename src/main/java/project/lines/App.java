package project.lines;

import java.io.*;
import java.util.ArrayList;


/**
 * Программа считывает файл построчно и записывает в новый файл каждую десятую строку
 *
 */
public class App {
    public static void main(String[] args)  {
        ArrayList<String> list = new ArrayList<String>();
        String a;
        String newlist;
        try {
            BufferedReader br = new BufferedReader(new FileReader("D:\\Downloads\\List.txt"));
                while ((a = br.readLine()) != null) {
                list.add(a);
            }
        } catch (IOException e) {
            System.err.println("ошибка чтения с файла");
        }

        try {
            BufferedWriter fw = new BufferedWriter(new FileWriter("D:\\Downloads\\newList.txt"));
            for (int i = 9; i < list.size(); i += 10) {
                newlist=list.get(i)+"\r\n";
                fw.write(newlist);
                fw.flush();
                System.out.println(newlist);
            }
        } catch (IOException e) {
        System.err.println("ошибка записи в файл");
        }
    }
}